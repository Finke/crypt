<?php 

/*
 _________WARNING: ___________________________
This Script is still in development. It's rough coded but it is working.
Use at your own risk.
_________________________MOBILE VERSION______
*/

session_start();

#_________ONLY MAKE CHANGES HERE!_________
$folder = "dateien";  // create this folder manually, please. In the same folder as index.php 
$reverse = 1;   // changes the order of readable files
date_default_timezone_set("Europe/Berlin"); // feel free to change it to your needs.
$save_to_file = 1; // set to 1 if you want to save the files locally and not to store them into your webspace
$read_from_input = 1; // set to 1 if you want to read the files from input and not from your webspace
#____________________________________________

if(!is_dir($folder)){mkdir($folder);}; // seems that it does not function on some host. Therefore, please create it manually.

function encryptIt( $q ) {
     if(isset($_POST['key'])) {$cryptKey = $_POST['key'];} else {$cryptKey  = $_SESSION['key'];} 
	$qEncoded      = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), $q, MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ) );
    return( $qEncoded );
}

function decryptIt( $q ) {
    if(isset($_POST['key'])) {$cryptKey = $_POST['key'];} else {$cryptKey  = $_SESSION['key'];} 
    $qDecoded      = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $q ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
    return( $qDecoded );
}

session_regenerate_id();
$new_sessionid = session_id();
$filename = substr($new_sessionid,0,-15).".note";
$filename = date('dmyHis').$filename;


if (isset($_POST['key1'])){$_SESSION['key1'] = $_POST['key1']; }
if (isset($_POST['key2'])){$_SESSION['key2'] = $_POST['key2']; }
if (isset($_POST['blind'])) {$_SESSION['blind']  = 1;}
if (isset($_POST['key3'])){$_SESSION['key3'] = $_POST['key3']; $_SESSION['key'] = $_POST['key3'];}
if (isset($_POST['text'])){$_SESSION['textinsert'] = $_POST['text']; }
if(isset($_POST['fileread'])){$_SESSION['fileread'] = $_POST['fileread']; }
if(isset($_GET['decision'])){$_SESSION['decision'] = $_GET['decision'];}
if(isset($_GET['reload'])){ session_destroy();	header('Location: index.php');	exit;	}

if(!isset($_SESSION['textinsert'])){  // Prevents to print the css into the file
	echo'
<html>
<head>
<style type="text/css">
html {
	font-size:4em;
	}
	input {
		height: 4vh;
		font-size:2vh;
	}
	
	a:link, a:visited,a:hover,a:active {
		text-decoration:none;
		color:black;
		}
	
	textarea {
		height: 50vh;
		width:100vw;
		font-size:4vh;	
	}
	</style>
	
</head>
';
}


if(!isset($_SESSION['decision'])){
	echo '<a href="index.php?decision=write">write</a><br>';
	echo '<a href="index.php?decision=read">read</a><br>';
	die;
}
	
if($_SESSION['decision'] == "read"){
	if(isset($read_from_input) AND $read_from_input == 1) {
		if(!isset($_POST['input'])){
			echo '
			<form action="index.php" method="POST">
			<textarea name="input" autofocus required placeholder="Please enter your text."></textarea><br>
			<input type="password" name="key" required placeholder="Enter the Password"><br>
			<input type="submit">
			</form>
			';
			echo '<br><a href="index.php?reload">reset page</a>';
			die;
		}

		if(isset($_POST['input'])){
			$text = decryptIt($_POST['input']);
			echo '
			<form action="index.php" method="POST">
			<textarea name="input" autofocus required placeholder="Please enter your text.">
			';
			echo $text;
			echo '
			</textarea><br>
			<input type="password" name="key" required placeholder="Enter the Password"><br>
			<input type="submit">
			</form>'
			;
			session_destroy();
			echo '<br><a href="index.php?reload">reset page</a>';
			die;
			
		}
}

	
	
	
	if(!isset($_SESSION['fileread'])){
		if ( is_dir ( $folder ))
		{
			if ((count(scandir($folder)) - 2)<1){
				echo "There is no file to show. Please create a file first.";session_destroy();
				echo '<br><a href="index.php?reload">reset page</a>';
				die;
				}
			if ( $handle = opendir($folder) )
			{   
				echo '<form action="index.php" method="POST">  <label>File:    <select name="fileread" size="">';
				$datei=array();
				while (($file = readdir($handle)) !== false)
					{
						if($file <> ".") {
							if($file <> "..") {
								array_push($datei,$file);
								}}
					}
				closedir($handle);
				if($reverse == 1) { $datei = array_reverse($datei); }
				$count = 0;
				while($count < count($datei))
				{
					echo '<option>'. $datei[$count].'</option>';
					$count++;
				}
			}
		}
		echo '    </select>  </label>  <input type="submit"></form>';
		die;
	}
}

if(isset($_SESSION['fileread'])) {
	if(!isset($_SESSION['key3'])) {
		echo'
		<form action="index.php" method="POST"> 
		<input type="password" name="key3" autofocus autocomplete="off" required verbatim placeholder="Choose a password">
		<input type="submit">
		</form>
		';
		die;
	}	
		
	
$readof = $folder."/".$_SESSION['fileread'];


	$show = file_get_contents($readof);
	$show = decryptIt($show);
	$show = str_replace ("\n","<br>",$show);
	echo $show;
	session_destroy();
	die;
	}

if(!isset($_SESSION['key1'])) {
	echo'
	<form action="index.php" method="POST"> 
	<input type="password" name="key1" autofocus autocomplete="off" required verbatim placeholder="Choose a password.">
	<input type="submit">
	</form>
	';
	die;
}

if(!isset($_SESSION['key2'])) {
	echo'
	<form action="index.php" method="POST"> 
	<input type="password" name="key2" autofocus autocomplete="off" required verbatim placeholder="Confirm your password.">
	<br>
	Do you want to write blind (in asterisks)? <input type="checkbox" name="blind" value="1"><br>
	<input type="submit">
	</form>
	';
	die;
}

if ($_SESSION['key1'] <> $_SESSION['key2']) { 
	echo "The passwords doesn't match. Please load this site anew."; 
	session_destroy();
	echo '<br><a href="index.php?reload">reset page</a>';
	die;
	}

$_SESSION['key'] = $_SESSION['key2'];

if(!isset($_SESSION['textinsert'])){ 
	echo '
	<form action="index.php" method="POST">
	';
	if(isset($_SESSION['blind'])){
		echo '
			<input type="password" style="width:500px;height:500px;" name="text" autofocus required placeholder="Please enter your text."><br>
		';
		}
	else {	
		echo '
		<textarea name="text" autofocus required placeholder="Please enter your text."></textarea><br>
		';
	}
	echo '
	<input type="submit">
	</form>
	';
	echo '<br><a href="index.php?reload">reset page</a>';
	die;
	}

if(isset($_SESSION['textinsert'])){ 

	
	$datum= date('d.m.Y\ \u\m\ H:i:s\ \U\h\r'); // German Time format
	// $datum= date('d.m.Y\ \a\t\ H:i:s'); // dd.mm.yy at hh:mm:ss  -> Change it to your needs.
	$en = $datum."\n".$_SESSION['textinsert'];
	$en = encryptIt($en);
	$de = decryptIt($en);
	
	if(isset($save_to_file) AND $save_to_file == 1){
		header("Content-disposition: attachment; filename=".$filename);
		header("Content-type: text/plain");
		echo $en;
		session_destroy();
		die;
	}
	
	echo $filename."<br>";
	file_put_contents($folder."/".$filename,$en);
	echo "<hr>";
	$show = file_get_contents($folder."/".$filename);
	echo decryptIt($show);
	session_destroy();
	die;
	}

echo '</html>';
die;
?>
